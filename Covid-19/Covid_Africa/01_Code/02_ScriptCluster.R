#Date: December 16 2019

rm(list =setdiff(ls(), c("Index")))

# Load libraries ----
library("data.table")
library("rstudioapi")
library("dplyr")
library("xml2")
library("foreach")
library("doParallel")

# Load information ----

####################################################################################
# 0.1 USERS input
# 0.1.1 Input reference simulations: ----
# Standard xml files for each scenario values are set to reference scenario values
startyear.r <- 2018
stopyear.r <- 2046
max.pop.r <- 1025
p1.r <- 0.26
mbr.r <- 600
xfoi.r <- 0.0025

# 0.1.2 Input main simulations:----


###################################################################################

base.dir <- dirname(dirname(rstudioapi::getActiveDocumentContext()$path))
data.dir <- file.path(base.dir, "02_Data")
ing.dir  <- file.path(base.dir, "03_Ingredients")
out.dir  <- file.path(base.dir, "04_Main_output")

###Create a folder 03_Ingredients per Scenario Index for cluster run ----
dir.create(file.path(base.dir, paste0("Ingredients_",Index)), showWarnings = FALSE)
ing.dir.s <- file.path(base.dir, paste0("Ingredients_",Index))
list_of_files <- list.files(ing.dir)
file.copy(file.path(ing.dir,list_of_files), ing.dir.s)


# 0.2 Load MDA and bednets pattern and importation data----
MDAdata <- as.data.table(read.csv(file.path(data.dir, "MDAcov19.csv"),sep=",",header=TRUE, stringsAsFactors=FALSE))
BNdata <- as.data.table(read.csv(file.path(data.dir, "BNcov.csv"),sep=",",header=TRUE, stringsAsFactors=FALSE))
parametersdata<-as.data.table(read.csv(file.path(data.dir, "parametersdata.csv"),header = TRUE))
scendef<- as.data.table(read.table(file.path(data.dir,"Scendef.csv"),sep=",",header=TRUE)) #importation year data

scen=as.character(scendef[SIndex==Index]$ScenarioName)

n <- nrow(parametersdata) #10000number of simulations in the main scenario
jobd<-as.numeric(nrow(parametersdata)) #20number of group of paramters to being assigned per cluster job
#########################################################################
# 1. Creates specific xml files per scenario ----
#########################################################################
# 1.1. Load standard reference scenario ----
root.ref <- read_xml(file.path(ing.dir, "root_ref.xml"))

#1.2. Modify coverage across all scenarios ----

#1.2.1. Add bed nets interventions----
bednets.expo<-xml_find_all(root.ref, ".//exposure.interventions")
bednets.cont<-xml_find_all(root.ref, ".//contribution.interventions")


#BNdata[ScenarioName == globalenv()$Index,]
#BNdata[ScenarioName == get("Index", envir = parent.frame()),]
bedval <- BNdata[SIndex==Index,
c("year","month","effectivity","fraction.excluded")]

for (j in 1:nrow(bedval)){
    
    bednets.expo%>%xml_add_child("moment",
    year = as.numeric(bedval[j,"year"]),
    month = as.numeric(bedval[j,"month"]),
    effectivity=as.numeric(bedval[j,"effectivity"]),
    fraction.excluded=as.numeric(bedval[j,"fraction.excluded"]))
    
    bednets.cont%>%xml_add_child("moment",
    year = as.numeric(bedval[j,"year"]),
    month = as.numeric(bedval[j,"month"]),
    effectivity=as.numeric(bedval[j,"effectivity"]),
    fraction.excluded=as.numeric(bedval[j,"fraction.excluded"]))
}

# 1.2.2 Add MDA coverage ----
MDA<-xml_find_all(root.ref, ".//treatment.rounds")

# Node1: xIA, Node2:IDA, Node3:xDA, Node4:xxA
drugs<-c("xIA","IDA","xDA","xxA")

for( d in 1:4 ){
    MDAd<-MDA[[d]]
    MDAval<-as.data.table(MDAdata[SIndex==Index&drug==drugs[d],
    c("year","month","coverage","fraction.excluded","delay")])
    
    
    if(nrow(MDAval)!=0){
        for (j in 1:nrow(MDAval)){
            MDAd%>%xml_add_child("treatment.round",
            year = as.numeric(MDAval[j,"year"]),
            month = as.numeric(MDAval[j,"month"]),
            coverage=as.numeric(MDAval[j,"coverage"]),
            fraction.excluded=as.numeric(MDAval[j,"fraction.excluded"]),
            delay=as.numeric(MDAval[j,"delay"]))
        }
        
    }
}
#1.3.  Write xml scenario file ----
write_xml(root.ref, file = file.path(ing.dir.s,paste0(scen,".xml")))


setwd(ing.dir.s)
time<-c(1851.99,2017.99:2045.99)
value=rep(1,length(time))
tvalue<-paste0(time,":",value)
xfoitrendv<-paste(tvalue, collapse=", ")
writeLines(xfoitrendv, paste0(scen,"_xfoitrend.txt") , useBytes=T)

#########################################################################
# 3. Run main simulations -----
#########################################################################

cluster <- makeCluster(20)
registerDoParallel(cluster)
output <- foreach(i = 1:jobd,
.errorhandling = "remove",
.packages = c("XML", "data.table","xml2","dplyr"),
.combine=rbind) %dopar% {
    
    root.ref <- read_xml(file.path(ing.dir.s,paste0(scen,".xml")))
    
    # 3.1 Create a folder 03_Ingredients per Scenario-Group Index for cluster run ----
    dir.create(file.path(base.dir, paste0("Ingredients_",Index,"_",i)), showWarnings = FALSE) #to add for cluster
    ing.dir.sg <- file.path(base.dir, paste0("Ingredients_",Index,"_",i))
    list_of_files <- list.files(ing.dir.s)
    file.copy(file.path(ing.dir.s,list_of_files), ing.dir.sg)
    
    # 3.2 Read paramater sets at group i ----
    parameterg<-parametersdata[i,]
    param.values<-paste0("-a=true -seed=0 -rbr=",parameterg$rbr," -pop=",parameterg$pop," -exp=",parameterg$exp," -xfoimult=",parameterg$xfoimult)
    param.loc<-paste0(ing.dir.sg,"/parameters.txt")
    writeLines(param.values, param.loc, useBytes=T)
    
    # 3.3 Read xml file and create a copy per i parmater set ----
    setwd(ing.dir.sg)
    write_xml(root.ref, file=file.path(ing.dir.sg, paste0(scen,"_",i,".xml")))
    if(.Platform$OS.type == "unix") {
        system(paste0("java -Xms400m -Xmx400m -XX:+UseG1GC -XX:MaxGCPauseMillis=5000 -cp .:./wormsim.jar:./colt.jar:./commons-math3-3.6.1.jar Wormsim -runbatch -parameters=parameters.txt -scenario=",scen,"_",i," -xfoitrend=",scen,"_xfoitrend.txt"),intern = T)
    }else{
        shell(paste0("java -cp .;wormsim.jar;colt.jar;commons-math3-3.6.1.jar Wormsim -runbatch -parameters=parameters.txt -scenario=",scen,"_",i," -xfoitrend=",scen,"_xfoitrend.txt"))
    }
    # 3.4 Generate the output of the scenario ----
    
    unzip(file.path(ing.dir.sg, paste0(scen,"_",i,".mflog.zip")))
    scen.data<-as.data.table(read.table(file.path(ing.dir.sg, paste0(scen,"_",i,".mflog")),header=TRUE))
    names(scen.data)<-gsub( '.', 'plus', names(scen.data),fixed = T)
    scen.data[,max.population.size:=round(popmult*max.pop.r,0)]
    scen.data[,mbr:=rbr*mbr.r]
    scen.data[,k:=p1exp]
    nrows<-nrow(scen.data[id==1])
    scen.data[,id:=NULL]
    scen.data[,foi.s:=rep(parameterg$xfoimult,each=nrows)] #foi from sampling
    #scen.data[,foi.sr:=rep(as.numeric(scen.data[year==2000]$xfoi),each=nrows)] same as foi.s
    scen.data[,id:=rep(parameterg$sim,each=nrows)]#simulation number
    unlink(ing.dir.sg,TRUE)
    output<-scen.data
}

stopCluster(cluster)
save(output,file = file.path(out.dir, paste0(scen,"_final.RData")))
unlink(ing.dir.s,TRUE)


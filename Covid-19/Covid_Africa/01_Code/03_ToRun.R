rm(list = ls())
library(rstudioapi)

#Run this part of the code if the XML files has not been previously generated .
dir.sent    <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(dir.sent)
source("01_PreScript.R",echo=TRUE)

for (Index in 1:13){
  #reference scenarios
  dir.sent    <- dirname(rstudioapi::getActiveDocumentContext()$path)
  setwd(dir.sent)
  source("02_ScriptCluster.R",echo=TRUE)
}



